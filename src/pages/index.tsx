import { graphql } from 'gatsby'
import * as React from 'react'
import Article from '../components/layout/article'
import Container from '../components/layout/container'
import HomeSlider from '../components/layout/home-slider'
import { formatPageTitle, useTitle } from '../utils'

interface IndexPageProps {
  data: {
    site: {
      siteMetadata: {
        name: string
        tagline: string
        title: string
      }
    }
  }
}

export const indexPageQuery = graphql`
  query IndexPageQuery {
    site {
      siteMetadata {
        name
        tagline
        title
      }
    }
  }
`

export default function IndexPage(props: IndexPageProps) {
  const { title } = props.data.site.siteMetadata

  const pageTitle = formatPageTitle(title)('Home')
  useTitle(pageTitle)

  return (
    <Container>
      <HomeSlider theme="DarkTheme">
        <Article>
          <h3>
            So you know my name is Guy. What else is there to know about me?
          </h3>
          <p>
            I'm a Christian, husband, father, software engineer, designer, and
            poet.
          </p>
        </Article>
      </HomeSlider>
    </Container>
  )
}
