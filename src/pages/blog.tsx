import { graphql, Link } from 'gatsby'
import * as React from 'react'
import Card from '../components/layout/card'
import Container from '../components/layout/container'
import { capitalize } from '../utils'
import setLeftOrRight from '../utils/card-utils'

interface BlogIndexPageProps {
  data: {
    allMdx: {
      edges: Array<{
        node: {
          id: string
          excerpt: string
          frontmatter: {
            title: string
            subject: string
          }
          slug: string
        }
      }>
    }
  }
}

interface IBlogPostPreview {
  excerpt: string
  id: string
  subject: string
  title: string
  slug: string
}

export const blogIndexPageQuery = graphql`
  query blogIndexPageQuery {
    allMdx {
      edges {
        node {
          id
          frontmatter {
            subject
            title
          }
          slug
          excerpt
        }
      }
    }
  }
`

export default function BlogIndexPage(props: BlogIndexPageProps) {
  const blogPosts = props.data.allMdx.edges

  // so gql may hand us some null or undefined subjects. We obviously don't want to show those.
  function filterNullUndefined<T = string | null | undefined>(val: T): boolean {
    if (typeof val === 'string') {
      const lower = val.toLocaleLowerCase()
      // if it's null, we don't want it.
      return !(lower === 'null' || lower === 'undefined')
    }
    return false
  }

  const postPreviews = blogPosts
    // filter out any posts without a subject
    .filter(
      node =>
        node.node.frontmatter.subject &&
        node.node.frontmatter.subject !== 'null'
    )
    // then flatten the structure for ease of use
    .map(node => {
      const { id, excerpt, slug, frontmatter } = node.node
      return {
        excerpt,
        id,
        slug,
        subject: frontmatter.subject,
        title: frontmatter.title,
      } as IBlogPostPreview
    })

  // all the subjects in allMdx. filters out undefined subjects
  const subjects: string[] = Array.from(
    new Set(
      blogPosts.map(({ node }) => {
        const { frontmatter } = node
        if (frontmatter.subject) {
          return frontmatter.subject
        }
      })
    )
  ).filter(filterNullUndefined) as string[]

  const subjectCollections = subjects.map(subject => (
    <>
      <h2 key={subject}>{capitalize(subject)}</h2>
      {postPreviews.map((post, index) => (
        <Link to={`../${post.slug}`} key={post.slug}>
          <Card
            text={{ blurb: post.excerpt, heading: post.title }}
            shadow={setLeftOrRight(index)}
          />
        </Link>
      ))}
    </>
  ))

  return (
    <Container>
      <h1>Blog Posts</h1>
      <p>
        Welcome! I'm delighted that you would consider taking some of your time
        to read my thoughts. I hope that you are encouraged, informed, or
        humored by what you encounter. Please feel free to contact me about
        anything you find. <Link to="../contact">Contact</Link>
      </p>
      <section>{subjectCollections}</section>
    </Container>
  )
}
