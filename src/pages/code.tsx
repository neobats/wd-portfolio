import React from 'react'
import stripes from '../assets/code_stripes.svg'
import Card from '../components/layout/card'
import LaunchPage from '../components/layout/launch-page'

const Code: React.FC = props => {
  return (
    <LaunchPage
      themeColor="blue"
      image={{
        alt:
          'A set of diagonal strokes as parallelograms. One blue, one yellow, and one red.',
        src: stripes,
      }}
      title="Code"
      subtitle={`
          Code is poetic communication between humans, computers, and other humans.
          It is the tool we steward for building tomorrow’s world.
          Here are some things I have built.
        `}
    >
      <Card
        shadow="left"
        text={{
          blurb: `A Java executable for scoring a non-profit organization's dog show.`,
          heading: 'Wooftacular',
        }}
      />
      <a href="https://github.com/neobats/react-todo-redux">
        <Card
          shadow="right"
          text={{
            blurb: `A todo app with React Hooks`,
            heading: 'React Todo',
          }}
        />
      </a>
    </LaunchPage>
  )
}

export default Code
