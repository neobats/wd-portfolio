import React from 'react'
import stripes from '../assets/write_stripes.svg'
import Card from '../components/layout/card'
import LaunchPage from '../components/layout/launch-page'

const Write: React.FC = props => {
  return (
    <LaunchPage
      themeColor="yellow"
      image={{
        alt: 'A set of horizontal stripes. One blue, one yellow, and one red.',
        src: stripes,
      }}
      title="Write"
      subtitle={`
        A thousand of the right words are worth more than a picture.
        Here’s my blog on tech, life, and design, which you could call
        Documentation-Driven Development for my life.
      `}
    >
      <Card
        shadow="left"
        text={{
          blurb: `An upcoming story on writing this blog in Gatsby.`,
          heading: 'Writing in Gatsby Under a Deadline',
        }}
      />
      <Card
        shadow="right"
        text={{
          blurb: `A villanelle about the trials and triumphs of marriage`,
          heading: 'A Love Supreme',
        }}
      />
    </LaunchPage>
  )
}

export default Write
