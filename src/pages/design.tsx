import React from 'react'
import stripes from '../assets/design_stripes.svg'
import unexperts from '../assets/unexperts.png'
import Card from '../components/layout/card'
import LaunchPage from '../components/layout/launch-page'

const Design: React.FC = props => {
  return (
    <LaunchPage
      themeColor="red"
      image={{
        alt: 'A set of diagonal stripes. One blue, one yellow, and one red.',
        src: stripes,
      }}
      title="Design"
      subtitle={`
        An expression of humanity;
        a tangible philosophy of life; an allegory of existence.
        Here are some of my thoughts you can read and see.
        `}
    >
      <Card
        shadow="left"
        text={{
          blurb: `UI/UX, logo, and graphic design for a podcast by three guys
        who know enough to know that they don't know enough.`,
          heading: 'The Unexperts Podcast',
        }}
        image={{
          alt: 'The Unexperts Logo',
          side: 'left',
          src: unexperts,
        }}
      />
    </LaunchPage>
  )
}

export default Design
