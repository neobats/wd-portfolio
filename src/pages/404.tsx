import { Link } from 'gatsby'
import React from 'react'
import Container from '../components/layout/container'
import * as styles from './404.module.scss'

const NotFound: React.FC = () => {

  return (
    <Container>
      <main className={styles.Main}>
        <div>
          <p className={styles.topLeft}>Well this is awkward.</p>
          <p className={styles.centerRight}>Looks like you ended up here on accident.</p>
          <p className={styles.center}>
            <Link to="/">Want to find your way home?</Link>
          </p>
          <h1 className={styles.topRight}>4</h1>
          <h1 className={styles.centerLeft}><span title="0">0</span></h1>
          <h1 className={styles.bottomCenter}>4</h1>
        </div>
      </main>
    </Container>
  )
}

export default NotFound
