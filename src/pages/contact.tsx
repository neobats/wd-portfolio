import React from 'react'
import triangles from '../assets/triangles.svg'
import unexperts from '../assets/unexperts.png'
import Card from '../components/layout/card'
import LaunchPage from '../components/layout/launch-page'

const Contact: React.FC = (props) => {

  return (
    <LaunchPage
      themeColor="red"
      image={{
        alt: 'A set of diagonal stripes. One blue, one yellow, and one red.',
        src: triangles,
      }}
      title="Contact"
      subtitle={`
        I’m the result of countless people pouring into me,
        and all my good results are because of those who came before me.
        I love to collaborate: to grow, to aid, and to flourish. Please reach out if I can assist you.
      `}
    />
  )
}

export default Contact
