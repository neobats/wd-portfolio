import React from 'react'

export default function tagify(tags: string | null) {
  if (!tags) {
    return null
  }
  return tags
    .replace(/ /g, '')
    .split(',')
    .map(tag => <em key={tag}>{`${tag} `}</em>)
}
