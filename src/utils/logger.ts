export function log(value: any) {
  console.log(value)
  return value
}
export function logFn(func: ([...any]) => any) {
  return (values: [...any]) => {
    const result = func(values)
    console.log(result)
    return result
  }
}
