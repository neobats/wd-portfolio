import Typography from 'typography'

const typography = new Typography({
  title: 'BattonBlog',
  baseFontSize: '16px',
  googleFonts: [
    {
      name: 'IBM Plex Sans',
      styles: ['400', '400 italic', '700'],
    },
    {
      name: 'IBM Plex Serif',
      styles: ['400', '400 italic', '700'],
    },
    {
      name: 'IBM Plex Mono',
      styles: ['400', '400 italic', '700'],
    },
  ],
  headerFontFamily: ['IBM Plex Serif', 'Georgia', 'serif'],
  bodyFontFamily: ['IBM Plex Sans', 'Helvetica', 'Arial', 'sans-serif'],
  overrideStyles: ({ adjustFontSizeTo, rhythm }, options, styles) => ({
    blockquote: {
      ...adjustFontSizeTo('.9em'),
      fontStyle: 'italic',
      paddingLeft: rhythm(13 / 16),
      marginLeft: rhythm(-1),
      borderLeft: `${rhythm(3 / 16)} solid #96721D`,
      backgroundColor: `#333333`,
    },
  }),
})

export default typography
