// import Maybe from './monads/maybe'
type LooseObject = {} | any
export type Pattern<T> = { [K in keyof T]: Pattern<T[K]> }
// type fun<T, S> = (some?: T) => S

export interface Matcher<T, S> {
  with: <P extends Pattern<T>>(pattern: P, expression: (some?: T) => S) => Matcher<T, S>
  _: (defaultFn: () => S | null) => Matcher<T, S>
  exec: () => S | null
}

export const match: <Value, Type>(needle: Value) => Matcher<Value, Type>
  = <a, b>(needle: a) => matcher<a, b>(needle)(() => null, [])

const matcher: <T, S>(value: T)
  => (defaultFunc: () => S | null, patterns: Array<[Pattern<T>, (some?: T) => S]>)
    => Matcher<T, S>
  = <T, S>(value: T) => (defaultFunc: () => S | null, patterns: Array<[Pattern<T>, (some?: T) => S]> = []) => ({
    // an object as parameter
    with: <p extends Pattern<T>>(
      // takes a pattern and an expression
      pattern: p,
      expression: (some?: T) => S,
    ) => matcher(value)(defaultFunc, [...patterns, [pattern, expression]]),

    _: (defaultFn: () => S | null) => matcher(value)(defaultFn, patterns),

    exec: (): S | null => {
      const p: [Pattern<T>, (some?: T) => S] | undefined = patterns.find(val => matchPattern(value, val[0]))
      return p ? p[1](value) : defaultFunc()
    },
  } as Matcher<T, S>)

const matchPattern: <a extends LooseObject >(needle: a, pattern: Pattern<a>) => boolean
  = (needle, pattern) => {
    if (typeof (needle) !== 'object') {
      return needle === pattern
    } else {
      return Object.keys(pattern).every((k: keyof Pattern<LooseObject>) =>
        pattern[k] === null ? false : matchPattern(needle[k], pattern[k]),
      )
    }
  }
