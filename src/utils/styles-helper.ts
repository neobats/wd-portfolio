import { format } from './formatter'
import { ColorTheme } from './types'

export interface StyleModule {
  [className: string]: string
}

const addStylesUnsafe = (...args: string[]) => args.length > 1 ? args.join(' ') : args[0]
export function addStyles(...styles: Array<string | undefined>): string {
  const someStyles = styles.filter((style: string | undefined) => typeof style !== 'undefined' ? style : '') as string[]
  return addStylesUnsafe(...someStyles)
}

export function toggleThemeStyles(lights: ColorTheme, style: StyleModule, additionalStyles?: string[]) {
  if (additionalStyles) {
    return lights.theme === 'light'
      ? addStyles(style.LightTheme, ...additionalStyles)
      : addStyles(style.DarkTheme, ...additionalStyles)
  } else {
    return lights.theme === 'light'
      ? addStyles(style.LightTheme)
      : addStyles(style.DarkTheme)
  }
}

// don't know if this works. RIP unit tests
export const addAndFormat = ((formatter: typeof format) => (formatFn: (phrase: string) => string) => {
  return (...args: Array<string | undefined>) => {
    return args.reduce((acc, curr) => {
      if (typeof curr === 'undefined') {
        return acc
      } else {
        return addStyles(formatter(curr, {
          formatter: formatFn,
          homeText: '',
          link: false,
        })), addStyles('')
      }
    }, '')
  }
})(format)
