export interface FormatterOptions {
  formatter: (phrase: string) => string
  link?: boolean
  homeText?: string
  nav?: boolean
}

export const formatPageTitle = (query: string) => (title: string) =>
  formatCurried({ formatter: (phrase: string) => phrase })(
    `${title} - ${query}`
  )

const slugify = (phrase: string) => phrase.toLowerCase().split(' ').join('-')

export const capitalize = (word: string) =>
  word[0].toLocaleUpperCase().concat(word.slice(1)).trim()

export const formatString = ((
  capitalizer: (word: string) => string
): ((phrase: string) => string) => {
  return (phrase: string) =>
    phrase
      .split(' ')
      .map(val => (val ? capitalizer(val) : ''))
      .join(' ')
      .trim()
})(capitalize)

const defaultFormat: FormatterOptions = {
  formatter: formatString,
  homeText: 'home',
}

export function format(
  item: string,
  { link, homeText, formatter }: FormatterOptions = defaultFormat
): string {
  switch (item) {
    case homeText:
      return link ? '/' : formatter(item)
    default:
      return link ? `../${slugify(item)}` : formatter(item)
  }
}
export const formatLink = ((formatter: typeof format) => (
  item: string,
  homeText = defaultFormat.homeText
) =>
  formatter(item, {
    formatter: defaultFormat.formatter,
    link: true,
    homeText,
  }))(format)

export const formatCurried = (options: FormatterOptions) => (phrase: string) =>
  format(phrase, options)
