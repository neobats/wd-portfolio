import { Colors } from './color-theme'

export enum BtnRole {
  cta = 'cta',
  primary = 'primary',
  secondary = 'secondary',
  default = 'default',
}

interface Image {
  type: 'svg' | 'img'
  src: string
}

export interface Button {
  role: BtnRole,
  color: Colors,
  image?: Image
}
