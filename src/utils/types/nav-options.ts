export enum NavOptions {
  index = 'welcome home',
  code = 'code',
  design = 'design',
  write = 'write',
  contact = 'contact',
}
