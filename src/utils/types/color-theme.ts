export interface ColorTheme {
  theme: Themes,
  toggle: () => void,
}

export enum Themes {
  dark = 'dark',
  light = 'light',
}

export enum Colors {
  yellow = 'yellow',
  red = 'red',
  blue = 'blue',
  gray = 'gray',
}
