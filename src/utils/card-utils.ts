export default function setLeftOrRight<T = 'left' | 'right'>(
  value: number | string | T
): 'left' | 'right' {
  if (typeof value === 'number') {
    return value % 2 === 0 ? 'left' : 'right'
  }
  switch (value) {
    case 'right':
      return 'right'
    default:
      return 'left'
  }
}
