// adapted from https://codewithstyle.info/advanced-functional-programming-in-typescript-maybe-monad/
export default class Maybe<T> {

  public static some<T>(value: T): Maybe<T> {
    if (!value) {
      throw TypeError(`I require a non-empty value, but I received: ${value}`)
    }
    return new Maybe(value)
  }

  public static none<T>(): Maybe<T> {
    return new Maybe<T>(null)
  }

  public static fromValue<T>(value: T): Maybe<T> {
    return value ? Maybe.some(value) : Maybe.none<T>()
  }
  private constructor(private value: T | null | undefined) { }

  public map<S>(fn: (wrapped: T) => S): Maybe<S> {
    return this.isNullOrUndefined(this.value) ? Maybe.none<S>() : Maybe.fromValue(fn(this.value as T))
  }

  public flatMap<S>(fn: (wrapped: T) => Maybe<S>): Maybe<S> {
    return this.isNullOrUndefined(this.value) ? Maybe.none<S>() : fn(this.value as T)
  }

  private getOrElse(defaultValue: T) {
    return this.isNullOrUndefined(this.value) ? defaultValue : this.value
  }

  private isNullOrUndefined(someValue: T | null | undefined): boolean {
    return someValue === null || someValue === undefined
  }
}
