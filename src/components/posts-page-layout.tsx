import { MDXProvider } from '@mdx-js/react'
import { graphql, Link } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import React from 'react'
import tagify from '../utils/tags'
import Container from './layout/container'
import * as styles from './posts-page.module.scss'

const shortcodes = { Link }
interface IProps {
  data: {
    mdx: {
      id: string
      body: string
      frontmatter: {
        title: string
        subject: string
        tags: string
      }
    }
  }
}

const PostPageTemplate: React.FC<IProps> = ({ data }) => (
  <Container>
    <div className={styles.Post}>
      <h1>{data.mdx.frontmatter.title}</h1>
      <article>
        <MDXProvider components={shortcodes as any}>
          <MDXRenderer>{data.mdx.body}</MDXRenderer>
        </MDXProvider>
      </article>
      <section>
        Tags:
        <br />
        {tagify(data.mdx.frontmatter.tags)}
      </section>
    </div>
  </Container>
)

export const pageQuery = graphql`
  query BlogPostQuery($id: String) {
    mdx(id: { eq: $id }) {
      id
      body
      frontmatter {
        title
        subject
        tags
      }
    }
  }
`
export default PostPageTemplate
