import * as styles from 'btn.module.scss'
import React from 'react'
import { addStyles, Button, formatString } from '../../../utils'

const Btn: React.FC<Button> = ({ children, role, color, image }) => {
  const formatButton = (r: string = 'default', c: string = 'blue') =>
    formatString(`.${r} .${c}`)

  const primaryAnimation = () => {
    return (null as unknown) as EventListener
  }
  const ctaAnimation = () => {
    return (null as unknown) as EventListener
  }
  const secondaryAnimation = () => {
    return (null as unknown) as EventListener
  }

  type Role = typeof role
  const matchButton = (r: Role) => {
    switch (r) {
      case 'primary':
        return primaryAnimation
      case 'cta':
        return ctaAnimation
      case 'secondary':
        return secondaryAnimation
    }
  }

  return (
    <div>
      <button className={addStyles(styles.button, formatButton(role, color))}>
        {image ? image : children}
      </button>
    </div>
  )
}

export default Btn
