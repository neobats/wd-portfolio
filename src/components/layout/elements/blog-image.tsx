import React from 'react'
import * as styles from './blog-image.module.scss'
interface BlogImageProps {
  src: string
  styles?: string[]
}

const BlogImage: React.FC<any> = props => {
  return <img src={props.src} className={styles.BlogImage}></img>
}
