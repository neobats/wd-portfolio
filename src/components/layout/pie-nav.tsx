import { motion } from 'framer-motion'
import React, { useState } from 'react'
import { Colors } from '../../utils'
import NavItem from './nav-item'

const PieNav: React.FC = ({ children }) => {
  const colors = ['yellow', 'red', 'blue']
  const [selected, setSelected] = useState(false)

  return (
    <nav>
      <ul>
        {colors.map((color, i) => (
          <NavItem
            key={`${i}${color}`}
            color={color as Colors}
            selectState={{ selected, setSelected }}
          >
            {children}
          </NavItem>
        ))}
      </ul>
    </nav>
  )
}

export default PieNav
