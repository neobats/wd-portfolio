import { MDXProvider } from '@mdx-js/react'
import { Link } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import React from 'react'
import Container from './container'

const shortcodes = { Link }

const BlogPostPage: React.FC<{ data: any }> = ({ children, data }) => {

  return (
    <Container>
      <MDXProvider components={shortcodes as any}>
        <MDXRenderer>{data}</MDXRenderer>
      </MDXProvider>
      {children}
    </Container>
  )
}

export default BlogPostPage
