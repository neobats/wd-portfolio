import React from 'react'
import codeIcon from '../../assets/code_icon.svg'
import designIcon from '../../assets/design_icon.svg'
import writeIcon from '../../assets/write_icon.svg'
import { addStyles } from '../../utils'
import * as styles from './home.module.scss'
import Title from './title'

interface Props {
  theme: 'LightTheme' | 'DarkTheme'
}

const HomeSlider: React.FC<Props> = ({ theme, children }) => {
  // come back and redo this with static queries and sitemetadata
  const keywordSpans = ['Code', 'Design', 'Write']
  const svgs = keywordSpans.map(span => {
    switch (span) {
      case 'Code':
        return codeIcon
      case 'Design':
        return designIcon
      case 'Write':
        return writeIcon
      default:
        return ''
    }
  })

  return (
    <div className={addStyles(theme, styles.Home)}>
      <div className={styles.box}>
        <Title>
          <p className={styles.Text}>
            {'Hey there! \n'}
            {`I'm Guy and I like to`}
          </p>
          <div className={styles.SliderChild}>
            <ul className={styles.flip3}>
              {keywordSpans.map((word, index) => (
                <li key={`${word}${index}`} className={styles[`word${index}`]}>
                  {word}
                  <img src={svgs[index] ? svgs[index] : ''} />
                </li>
              ))}
            </ul>
          </div>
        </Title>
      </div>
      {children}
    </div>
  )
}

export default HomeSlider
