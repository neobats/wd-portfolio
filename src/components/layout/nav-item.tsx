import { motion } from 'framer-motion'
import React, { Dispatch, SetStateAction } from 'react'
import { BtnColor, BtnRole, Button } from '../../utils'
import Btn from '../btn'

interface NavItemProps {
  color: BtnColor,
  selectState: {
    selected: boolean,
    setSelected: Dispatch<SetStateAction<boolean>>,
  },
}
const NavItem: React.FC<NavItemProps> = ({ children, color, selectState }) => {
  const { selected, setSelected } = selectState
  const navButton: Button = {
    color,
    role: 'primary' as BtnRole,
  }
  return (
    <li>
      <div>
        <Btn role={navButton.role} color={navButton.color}>
          {children}
        </Btn>
      </div>
    </li>
  )
}

export default NavItem
