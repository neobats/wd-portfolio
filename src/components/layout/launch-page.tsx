import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { addStyles, formatPageTitle, useTitle } from '../../utils'
import Container from './container'
import * as styles from './launch-page.module.scss'

interface LaunchPageProps {
  title: string,
  subtitle: string,
  image: {
    src: string,
    alt: string,
  }
  themeColor: 'red' | 'yellow' | 'blue'
}

const LaunchPage: React.FC<LaunchPageProps> = ({ children, title, subtitle, image, themeColor }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `,
  )

  useTitle(formatPageTitle(data.site.siteMetadata.title)(title))

  return (
    <Container>
      <div className={addStyles(styles.Page, styles[themeColor])}>
        <div className={styles.wrapped}>
          <h1>{title}</h1>
          <section>{subtitle}</section>
        </div>
        <img src={image.src} alt={image.alt}/>
        {children}
      </div>
    </Container>
  )
}

export default LaunchPage
