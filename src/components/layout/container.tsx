import React, { useState } from 'react'
import lightsOffBox from '../../assets/lights-off.svg'
import lightsOnBox from '../../assets/lights-on.svg'
import { toggleThemeStyles } from '../../utils/styles-helper'
import { ColorTheme, NavOptions, Themes } from '../../utils/types'
import * as styles from './container.module.scss'
import * as globalTheme from './home.module.scss'
import Nav from './nav/nav'

const Container: React.FC = ({ children }) => {
  const { dark, light } = Themes
  const [lightsOn, setLightsOn] = useState<Themes>(dark)
  const links: NavOptions[] = [
    'home',
    'code',
    'design',
    'write',
    'contact',
  ] as NavOptions[]

  const toggleLights = () => setLightsOn(lightsOn === dark ? light : dark)

  const generateLightsToggle: (lights: ColorTheme) => JSX.Element = lights => {
    if (lights.theme === 'light') {
      return (
        <button className={styles.ToggleLights} onClick={lights.toggle}>
          <img src={lightsOffBox} alt="turn off the lights" />
        </button>
      )
    } else {
      return (
        <button className={styles.ToggleLights}>
          <img src={lightsOnBox} alt="turn on the lights" />
        </button>
      )
    }
  }

  return (
    <div
      className={toggleThemeStyles(
        { theme: lightsOn, toggle: toggleLights },
        globalTheme,
        [styles.Container]
      )}
    >
      <nav>
        <Nav items={links}>
          <li>
            <div>
              {generateLightsToggle({ theme: lightsOn, toggle: toggleLights })}
            </div>
          </li>
        </Nav>
      </nav>
      {children}
      <footer>
        Made with ❤ in Missouri, USA
        <br />
        Copyright &copy; G. A. Batton and Neobats Web Solutions 2020
        <br />
        <a href="https://en.wikipedia.org/wiki/Soli_Deo_gloria">
          Soli Deo Gloria
        </a>
      </footer>
    </div>
  )
}

export default Container
