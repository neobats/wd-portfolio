import React from 'react'
import { addStyles } from '../../utils'

const Title: React.FC<{ theme?: string }> = ({ theme, children }) => (
  <h1 className={addStyles(theme)}>{children}</h1>
)

export default Title
