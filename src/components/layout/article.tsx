import React from 'react'
import * as styles from './article.module.scss'

const Article: React.FC = ({children}) => {
  return (
    <article className={styles.Article}>
      {children}
    </article>
  )
}

export default Article
