import React from 'react'
import * as styles from './card.module.scss'

interface Image {
  src: string
  side: 'left' | 'right'
  alt: string
}
interface CardProps {
  image?: Image
  shadow: 'left' | 'right'
  text: {
    heading: string
    blurb: string
  }
}

const Card: React.FC<CardProps> = ({ children, image, shadow, text }) => {
  const renderImage = (img?: Image) => {
    if (img) {
      return <img src={img.src} className={styles[img.side]} alt={img.alt} />
    }
  }

  const addSide = (condition: string | undefined) => {
    switch (condition) {
      case 'right':
        return 'Left'
      case 'left':
        return 'Right'
      default:
        return 'Middle'
    }
  }

  return (
    <div
      className={`${styles.Card}  ${
        shadow === 'right' ? styles.ShadowRight : styles.ShadowLeft
      }`}
    >
      {renderImage(image)}
      <article className={addSide(image?.side as 'left' | 'right')}>
        <h3>{text.heading}</h3>
        <em>{text.blurb}</em>
      </article>
      {children}
    </div>
  )
}

export default Card
