import { Link } from 'gatsby'
import React, { useState } from 'react'
import codeIcon from '../../../assets/code_icon.svg'
import designIcon from '../../../assets/design_icon.svg'
import writeIcon from '../../../assets/write_icon.svg'
import { Colors, format, formatLink } from '../../../utils'
import * as styles from './nav.module.scss'

const Nav: React.FC<{ items: string[] }> = ({ items, children }) => {
  const [selected, setSelected] = useState('welcome home')

  // this is for button compatibility
  const matchProps: (item: string) => { color: Colors; svg: string } = item => {
    switch (item) {
      case 'code':
        return {
          color: 'blue' as Colors,
          svg: codeIcon,
        }
      case 'design':
        return {
          color: 'red' as Colors,
          svg: designIcon,
        }
      case 'write':
        return {
          color: 'yellow' as Colors,
          svg: writeIcon,
        }
      case 'contact':
        return {
          color: 'gray' as Colors,
          svg: '',
        }
      default:
        return {
          color: 'white' as Colors,
          svg: '',
        }
    }
  }

  const activeStyles = ((matcher: typeof matchProps) => (item: string) => {
    const matchStyles = (keyword: string) => {
      switch (keyword) {
        case 'red':
          return '#962B15'
        case 'yellow':
          return '#D6A229'
        case 'blue':
          return '#0C7489'
        case 'gray':
          return '#333333'
        default:
          return '#4f4f4f'
      }
    }

    return {
      backgroundColor: matchStyles(matcher(item).color),
    }
  })(matchProps)

  const navChildren = items.map(matchProps)

  const maybeSvg = (svg: string) => {
    if (svg) {
      return <img src={svg} />
    }
  }

  // this doesn't work. ¯\_(ツ)_/¯. No unit tests, no reliability.
  // const addAndFormatNav = (condition: boolean) => addAndFormat(
  //   (phrase: string) => condition
  //     ? phrase.slice(0).toLocaleUpperCase().concat(phrase.slice(1))
  //     : '',
  // )

  return (
    <ul className={styles.NavList}>
      {items.map((item, index) => (
        <li
          key={`${item}${index}`}
          // className={`${styles.NavLink} ${styles[format(item)]}`}
          className={styles.NavLink}
          // onClick={setSelected(item) as any}
        >
          <Link to={formatLink(item)} activeStyle={activeStyles(item)}>
            {maybeSvg(navChildren[index].svg)}
            <figcaption>{format(item)}</figcaption>
          </Link>
        </li>
      ))}
      {children}
    </ul>
  )
}

export default Nav
