import React, { useEffect } from 'react'

const CremaApplication: React.FC<{ src: string }> = ({ src }) => {
  let resume = ''
  useEffect(() => {
    if (!src.endsWith('.json')) {
      resume = `Bummer. I couldn't parse this path: ${src}. Is it something other than JSON?`
    }
    fetch(src)
      .then(response => response.json())
      .then((parsable: JSON) => {
        resume = JSON.stringify(parsable)
      })
  }, [resume, src])

  return <article style={{ margin: 'auto' }}>{resume}</article>
}

export default CremaApplication
