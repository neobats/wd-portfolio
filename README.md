# Guy's Portfolio Site

## Made with ❤ and ⚡ with Gatsby

Protip: You can view a live version of this site by going to this URL

> [https://wd-portfolio.netlify.app/](https://wd-portfolio.netlify.app/)

This is my portfolio site. I wrote it in Gatsby, with MDX, TS, Sass, and a couple other plugins. Since it has been built in the wonderfully elegant Gatsby ecosystem, you'll need a few things before you start running this locally.

### How Do I Run This

First, you need the Gatsby CLI. You can find that [here](https://www.gatsbyjs.org/docs/quick-start). You'll need [Node.js](nodejs.org), and an Internet connection.
But since you're reading this, I'm assuming you already have an internet connection.  
After all that's done, you'll navigate to where you want to put this project and run `npm install`. This will take a while.

### But How Do I Run It

You can run this project a couple ways, and all of that is recorded in the Gatsby CLI. The easiest way to get started is with this command
`gatsby develop`, which will open up the web server on your local host, port 8000 (localhost:8000 in your browser). Then you can poke around!

# Note to self

Make sure that when you do layout for blog posts, you adjust to a two column layout for code snippets and images, so that the content about the asset can be next to and read in parallel with the asset.
