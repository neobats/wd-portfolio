module.exports = {
  siteMetadata: {
    title: `Guy Batton's Web Home`,
    name: `A Portfolio for Code, Design, and Writing`,
    tagline: `Guy Batton's portfolio site made with Gatsby.js`,
  },
  plugins: [
    `gatsby-plugin-sass`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-tslint`,
    `gatsby-plugin-mdx`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: `${__dirname}/src/blog`,
      },
    },
  ],
}

// note to self:
// use this for code page? https://www.gatsbyjs.org/packages/gatsby-remark-prismjs/
