const { createFilePath } = require('gatsby-source-filesystem')
const path = require('path')

// https://www.gatsbyjs.com/docs/mdx/programmatically-creating-pages/#generate-slugs
exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions
  if (node.internal.type === 'MDX') {
    const slugValue = createFilePath({ node, getNode, basePath: 'blog' })

    createNodeField({
      name: 'slug',
      node,
      value: `${slugValue}`,
    })
  }
}

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { data, errors } = await graphql(`
    query {
      allMdx {
        edges {
          node {
            id
            slug
          }
        }
      }
    }
  `)

  if (errors) {
    console.log(errors)
    reporter.panicOnBuild(`Oof. ${result}`)
  }

  const posts = data.allMdx.edges

  posts.forEach(({ node }, index) => {
    actions.createPage({
      path: node.slug,
      component: path.resolve(`./src/components/posts-page-layout.tsx`),
      context: { id: node.id },
    })
  })
}
